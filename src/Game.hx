import cdb.Types;

@:publicFields
class Game extends hxd.App { 
    
    var tgs : Array<h2d.TileGroup>;
    public var scroll : h2d.Object;
    public var root : h2d.Layers;
    var game : Game;

    override function init() {
        
        game = Game.inst;
        scroll = new h2d.Object(game.s2d);
        scroll.setScale(4);
		root = new h2d.Layers(scroll);

        tgs = [];

        // var tf = new h2d.Text(hxd.res.DefaultFont.get(), s2d);
        // tf.text = "Hello World !";

        var data = Data.levelData.all[0];
		var width: Int = data.width;
        var height: Int = data.height;
        var curLayer = 0;
		var t = hxd.Res.tiles.toTile();
		var tiles = t.gridFlatten(8);

        for (l in data.layers) {
            var d = l.data.data.decode();
            var tg = new h2d.TileGroup(t);
            // var lname = l.name;
            // trace(lname);
            tgs.push(tg);
            root.add(tg, curLayer);

            for (y in 0...height) {
                for (x in 0...width) {
                    var v = d[x + y * width] - 1;
                    if (v < 0) continue;
                    tg.add(x*8, y*8, tiles[v]);
                }
            }
        }
    }

    public static var inst: Game;

    static function main() {
        #if js
        hxd.Res.initEmbed();
		#else
        hxd.Res.initLocal();
		#end
        Data.load(hxd.Res.data.entry.getBytes().toString());
        inst = new Game();
    }
}

